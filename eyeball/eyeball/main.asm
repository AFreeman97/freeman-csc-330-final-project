; Andy Freeman
; December 12, 2018
; Freeman_CSC330_FinalProject
; Make a caterpillar grow with a corresponding potentiometer value
.def				workhorse	= r21
.def				adc_value_low	= r22
.def				adc_value_high	= r23
.def				refresh_q	= r25

.cseg
.macro				set_pointer
				ldi		@0, low(@2)
				ldi		@1, high(@2)
.endmacro

;----start code segment----
.org				0x00
				rjmp		setup
.org				0x001C
				rjmp		ISR_TIMER0_COMPA
.org				0x002A
				rjmp		ISR_ADC
.org				0x0200

setup:		;one time actions
				;start screen
				rcall		OLED_initialize
				rcall		GFX_clear_array
				set_pointer	XL, XH, pixel_array*2
				rcall		OLED_refresh_screen

				;clear registers
				ldi		r18, 0
				ldi		r19, 0

				;set up ADC
				ldi		workhorse, 0b01000011			;enable ADC3
				sts		ADMUX, workhorse
				ldi		workhorse, 0b11101111			;prescalar of 128 with interrupt
				sts		ADCSRA, workhorse

				;set up TIMER0
				ldi		workhorse, 0b00000010
				out		TCCR0A, workhorse
				ldi		workhorse, 0b00000101
				out		TCCR0B, workhorse
				ldi		workhorse, 0b00000010
				sts		TIMSK0, workhorse
				ldi		workhorse, 130
				out		OCR0A, workhorse

				sei

loop:		;repeated actions
				rcall		GFX_clear_array				;clear previous array
				push		r18					;conserve adc reading
				ldi		r18, 0
				rcall		draw_body
				ldi		r18, 0
				rcall		draw_legs
				pop		r18					;conserve adc reading
				rcall		draw_head
				tst		refresh_q				;test if timer says it's time to refresh screen
				breq		refresh					;refresh screen if it's time
				rjmp		loop

refresh:	;refresh OLED screen if timer register is ready
				set_pointer	XL, XH, pixel_array*2
				rcall		OLED_refresh_screen
				inc		refresh_q				;reset timer check register
				rjmp		loop

draw_head:	;draw the moving head
				mov		r18, adc_value_low
				cpi		r18, 120
				brsh		draw_head_120

				set_pointer	ZL, ZH, Char_257*2			;draw antennae
				ldi			r19, 23
				rcall		GFX_set_array_pos
				rcall		GFX_set_shape
				rcall		GFX_draw_shape
				set_pointer	XL, XH, pixel_array*2
				set_pointer ZL, ZH, Char_002*2				;draw face
				ldi		r19, 31
				rcall		GFX_set_array_pos
				rcall		GFX_set_shape
				rcall		GFX_draw_shape
				set_pointer	XL, XH, pixel_array*2
				set_pointer	ZL, ZH, Char_256*2			;draw legs
				ldi		r19, 39
				rcall		GFX_set_array_pos
				rcall		GFX_set_shape
				rcall		GFX_draw_shape
				set_pointer	XL, XH, pixel_array*2
				rjmp		draw_blanks
	draw_blanks:	;draw blanks after the moving head
				push		workhorse
				ldi		workhorse, 8
				add		r18, workhorse
				pop		workhorse
				ldi		r19, 31					;hide body
				rcall		GFX_set_array_pos
				rcall		GFX_draw_blank
				set_pointer	XL, XH, pixel_array*2
				ldi		r19, 39 				;hide legs
				rcall		GFX_set_array_pos
				rcall		GFX_draw_blank
				set_pointer	XL, XH, pixel_array*2
				cpi			r18, 120
				brlo		draw_blanks
				ret

draw_head_120:	;draw the head if the ADC places r18 above 120
				ldi		r18, 120
				set_pointer	ZL, ZH, Char_257*2			;draw antennae
				ldi		r19, 23
				rcall		GFX_set_array_pos
				rcall		GFX_set_shape
				rcall		GFX_draw_shape
				set_pointer	XL, XH, pixel_array*2
				set_pointer ZL, ZH, Char_002*2				;draw face
				ldi		r19, 31
				rcall		GFX_set_array_pos
				rcall		GFX_set_shape
				rcall		GFX_draw_shape
				set_pointer	XL, XH, pixel_array*2
				set_pointer	ZL, ZH, Char_256*2			;draw legs
				ldi		r19, 39
				rcall		GFX_set_array_pos
				rcall		GFX_set_shape
				rcall		GFX_draw_shape
				set_pointer	XL, XH, pixel_array*2
				ret

draw_body:	;draw the full, unchanging body
				set_pointer	ZL, ZH, Char_219*2			;draw body
				ldi		r19, 31
				rcall		GFX_set_shape
				rcall		GFX_set_array_pos
				rcall		GFX_draw_shape
				set_pointer	XL, XH, pixel_array*2
				push		workhorse
				ldi		workhorse, 8
				add		r18, workhorse
				pop		workhorse
				cpi		r18, 128				;check if drawer is at right edge of screen
				brlo		draw_body				;loop if not
				ret

draw_legs:	;draw all of the unchanging legs
				set_pointer	ZL, ZH, Char_256*2			;draw legs
				ldi		r19, 39
				rcall		GFX_set_array_pos
				rcall		GFX_set_shape
				rcall		GFX_draw_shape
				set_pointer	XL, XH, pixel_array*2
				push		workhorse
				ldi		workhorse, 24
				add		r18, workhorse
				pop		workhorse
				cpi		r18, 128				;check if drawer is at right edge of screen
				brlo		draw_legs				;loop if not
				ret


ISR_TIMER0_COMPA:	;TIMER0 ISR
				in		workhorse, SREG				;preserve status register
				ldi		refresh_q, 0				;set register to tell loop to branch to refresh
				out		SREG, workhorse				;preserve status register
				reti

ISR_ADC:	;ADC ISR
				in		workhorse, SREG				;preserve status register
				lds		adc_value_low, ADCL			;load ADCL into adc_value_low
				lds		adc_value_high, ADCH			;load ADCH into adc_value_high

				;left shift high value for OR with low
				lsl		adc_value_high
				lsl		adc_value_high
				lsl		adc_value_high
				lsl		adc_value_high
				lsl		adc_value_high

				;right shift low value for OR with high
				lsr		adc_value_low
				lsr		adc_value_low
				lsr		adc_value_low

				or		adc_value_low, adc_value_high
				out		SREG, workhorse				;preserve status register
				reti

.include		"lib_delay.asm"
.include		"lib_SSD1306_OLED.asm"
.include		"lib_GFX.asm"
